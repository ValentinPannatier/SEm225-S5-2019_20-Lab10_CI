#include "avg.h"
#include "unity.h"

#include <stdlib.h>
#include <string.h>

void setUp(void)
{
    /* set stuff up here */
}

void tearDown(void)
{
    /* clean stuff up here */
}

void test_avg_function(void)
{
    /* Check result */
    TEST_ASSERT_EQUAL_INT_MESSAGE(0, integer_avg(0, 0), "Error in avg of 0");
	TEST_ASSERT_EQUAL_INT_MESSAGE(3, integer_avg(4, 2), "Error in avg");
	TEST_ASSERT_EQUAL_INT_MESSAGE(4, integer_avg(4, 4), "Error in avg");
	TEST_ASSERT_EQUAL_INT_MESSAGE(3, integer_avg(3, 4), "Error in avg 3 et 4");

	
}

