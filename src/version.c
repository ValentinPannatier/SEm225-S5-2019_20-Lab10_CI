#include "version.h"

/* Version number */
#define API_MAJOR 1
#define API_MINOR 1
#define API_PATCH 1
#define VERSION "1.1.1"

/* Getters for version number */
 int get_version_major(void)
{
    return API_MAJOR;
}
 int get_version_minor(void)
{
    return API_MINOR;
}
 int get_version_patch(void)
{
    return API_PATCH;
}
const char* get_version_string(void)
{
    return VERSION;
}